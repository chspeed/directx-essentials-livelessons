###Overview

This is the source code repository for the video series *DirectX Essentials LiveLessons* by Paul Varcholik.

[![DirectX Essentials LiveLessons](http://www.varcholik.org/DirectXEssentialsLiveLessons/Cover.jpg)](http://my.safaribooksonline.com/video/illustration-and-graphics/9780134030036)

You'll find a forum for discussions and questions about the videos at [http://www.varcholik.org/](http://www.varcholik.org/).

###Dependencies

All lessons require Visual Studio 2013 (any sku, including [Express 2013 for Windows Desktop](http://www.visualstudio.com/en-us/products/visual-studio-express-vs.aspx)).


